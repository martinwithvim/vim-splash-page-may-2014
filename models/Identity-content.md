# Identity process

Identity is a major part of my work as a designer, here I outline my process with examples to ilustrate each step. Each project is unique, however, and I always endevour to find the right fit for each client.

## Research

<img> 
<caption> Visual design research using pinterest </caption>

Identifying the audience for the identity is key as is identifying the environment the brand will inhabit. Research is key to finding out about both the of these areas, as well as making early forays into design idiom and form. This is carried out by conversation with the client, my own research and opinion regarding audience and informal exploration of design styles.



## Sketching

<img> 
<caption> Sketches exploring early ideas for the Open ZFS brand </caption>

Sketching is often one of the most important steps in defining the concepts at play in the identity. By stepping away from the computer the focus of the sketch work is about defining the semantic meaning of the logo or type marque and less on the surface sheen and finish. It is also a fast way of exploring many different ways of tackling the design problem and finding a way towards a solution.

## Approaches

<img> 
<caption> A number of possible routes for the Dig Inclusion identity being worked up in Adobe Illustrator </caption>

From out of the sketches a number, often three or more, of approaches will emerge. In discussion with the client one or two are selected for development and at this point they are taken into the digital realm and worked up as vectors.

## Refinement

<img> 
<caption> Colour and supporting design elements are explored </caption>

At this point a final approach is chosen and colour and supporting elements are explored, this step can often blend with design concepts for a website or printed materials if these are also being produced for the client.

## Delivery

<img> 
<caption> A selection of identity marques designed with vim </caption>

Once the final marque is chosen I produce a full asset pack for the client with logos in a number of different file formats and variations. An accompanying identity guidelines document is produced to help ensure future applications of the identity are consistent.






