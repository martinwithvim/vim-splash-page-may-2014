# Vim
## design with vim

My name is Martin Coote and I am a graphic designer with vim. I have been working the design industry for around ten years, primarily in print design and production but increasingly in brand identity and digital design. Three fifths of the working week sees me with digital agency [fffunction](http://fffunction.co), and the rest of the time I work with my own clients and projects.

## case studies

### OpenZFS

Working with [fffunction](http://fffunction.co) I worked on the logo for OpenZFS an open source file system with the capability to handle zettabytes of information. A zettabyte is 10_21_ bytes and 10_21_ also happens to be the estimated number of grains of sand on earth. The logo contains an hourglass form comprised of one set of 10 dots and another or 21 contained within an abstracted z form.

[Find out more](openzfs.html)

### OpenZFS

Working with [fffunction](http://fffunction.co) I worked on the logo for OpenZFS an open source file system with the capability to handle zettabytes of information. A zettabyte is 10_21_ bytes and 10_21_ also happens to be the estimated number of grains of sand on earth. The logo contains an hourglass form comprised of one set of 10 dots and another or 21 contained within an abstracted Z form.

[Find out more](openzfs.html)

### OpenZFS

Working with [fffunction](http://fffunction.co) I worked on the logo for OpenZFS an open source file system with the capability to handle zettabytes of information. A zettabyte is 10_21_ bytes and 10_21_ also happens to be the estimated number of grains of sand on earth. The logo contains an hourglass form comprised of one set of 10 dots and another or 21 contained within an abstracted Z form.

[Find out more](openzfs.html)

## get in touch

If you have a project or idea that I could help with, i'd love to hear from you. Here are some ways you can find me:

- [twitter](http://twitter.com/martincoote)
- [skype](martincootedesign)

Or send me an email using the form below:

[your email]
[Message field]
[submit button]






